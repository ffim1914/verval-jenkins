package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.GradeValidator;
import validation.HomeworkValidator;
import validation.StudentValidator;
import validation.Validator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceTest {
    private static Service service;

    private static final String studentsXML = "test_students.xml";
    private static final String homeworksXML = "test_homework.xml";
    private static final String gradesXML = "test_grades.xml";

    @BeforeEach
    void createService() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        recreateFile(studentsXML, "Entities");
        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, studentsXML);
        recreateFile(homeworksXML, "Entities");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, homeworksXML);
        recreateFile(gradesXML, "Entities");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, gradesXML);

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    private static boolean deleteFileIfExists(String fileName) {
        File file = new File(fileName);
        if (file.exists() && file.isFile()) {
            return file.delete();
        }
        return true;
    }

    private static void recreateFile(String fileName, String entityType) {
        if (deleteFileIfExists(fileName)) {
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
                printWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
                printWriter.println("<" + entityType + "/>");
                printWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @ParameterizedTest
    @CsvSource({"id1,Eros Pista,931", "id2,Gyenge Gyula,532", "id81423,Edes Anna,430"})
    public void GivenCorrectData_WhenSavingStudent_ShouldSaveStudentData(String id, String name, int group) {
        int result = service.saveStudent(id, name, group);
        assertEquals(0, result);

        boolean studentFound = false;
        for (Student student: service.findAllStudents()) {
            if (id.equals(student.getID())) {
                studentFound = true;
                assertTrue(name.equals(student.getName()) && student.getGroup() == group, "Student has different data");
            }
        }
        assertTrue(studentFound, "Student was not saved");
    }

    @ParameterizedTest
    @CsvSource({"id1,Eros Pista,31", "id2,Gyenge Gyula,1002", "id81423,Edes Anna,-430"})
    public void GivenInvalidData_WhenSavingStudent_ShouldReturnOneAndNotSave(String id, String name, int group) {
        int result = service.saveStudent(id, name, group);
        assertEquals(1, result);

        boolean studentFound = false;
        for (Student student: service.findAllStudents()) {
            if (id.equals(student.getID())) {
                studentFound = true;
                assertFalse(name.equals(student.getName()) && student.getGroup() == group, "Student has same data");
            }
        }
        assertFalse(studentFound, "Student was saved");
    }

    @Test
    public void GivenStudentWithTakenId_WhenSavingStudent_ShouldReturnOneAndNotSave() {
        assertEquals(0, service.saveStudent("id1", "Pista", 623));
        assertEquals(1, service.saveStudent("id1", "Jozsi", 611));
    }

    @Test
    public void GivenIdThatExists_WhenDeletingStudent_ShouldDeleteStudent() {
        String id = "id9", name = "Jozsi";
        int group = 434;
        assertEquals(0, service.saveStudent(id, name, group));

        boolean studentFound = false;
        for (Student student: service.findAllStudents()) {
            if (id.equals(student.getID())) {
                studentFound = true;
                assertTrue(name.equals(student.getName()) && student.getGroup() == group, "Student has same data");
            }
        }
        assertTrue(studentFound, "Student was not saved");

        assertEquals(0, service.deleteStudent(id));

        studentFound = false;
        for (Student student: service.findAllStudents()) {
            if (id.equals(student.getID())) {
                studentFound = true;
                break;
            }
        }
        assertFalse(studentFound, "Student was not deleted");
    }

    @Test
    public void GivenIdIsNull_WhenDeletingStudent_ShouldReturnOne() {
        assertEquals(1, service.deleteStudent(null));
    }
}
