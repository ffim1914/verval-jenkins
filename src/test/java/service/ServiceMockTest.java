package service;

import domain.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ServiceMockTest {
    private Service service;

    private StudentXMLRepository studentXMLRepository;
    private GradeXMLRepository gradeXMLRepository;
    private HomeworkXMLRepository homeworkXMLRepository;

    @BeforeEach
    void createService() {
        studentXMLRepository = Mockito.mock(StudentXMLRepository.class);
        gradeXMLRepository = Mockito.mock(GradeXMLRepository.class);
        homeworkXMLRepository = Mockito.mock(HomeworkXMLRepository.class);

        service = new Service(studentXMLRepository, homeworkXMLRepository, gradeXMLRepository);
    }

    @Test
    public void GivenCorrectData_WhenSavingStudent_ShouldSaveStudentData() {
        String id = "id1", name = "Eros Pista";
        int group = 931;
        Student student = new Student(id, name, group);

        when(studentXMLRepository.save(student)).thenReturn(student);
        assertEquals(0, service.saveStudent(id, name, group));
        verify(studentXMLRepository, times(1)).save(any(Student.class));
    }

    @Test
    public void GivenStudentWithTakenId_WhenSavingStudent_ShouldReturnOneAndNotSave() {
        //Mockito
        Student student = new Student("id1", "Pista", 623);
        when(studentXMLRepository.save(student)).thenReturn(student);
        assertEquals(1, service.saveStudent("id1", "Jozsi", 611));
    }

    @Test
    public void GivenIdIsNull_WhenDeletingStudent_ShouldReturnOne() {
        when(studentXMLRepository.delete(null)).thenThrow(new IllegalArgumentException("ID is null!\n"));
        assertEquals(1, service.deleteStudent(null));
    }
}
